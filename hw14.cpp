#include <iostream>
#include <string>

using namespace std;

int main()
{
	string straightline("1234567112345");

	cout << straightline << "\n";
	cout << "\n" << straightline.length() << "\n";
	cout << straightline[0] << "\n";
	cout << straightline[straightline.size() - 1] << "\n";

}